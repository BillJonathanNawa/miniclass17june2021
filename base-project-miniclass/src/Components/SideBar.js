import React from 'react'
import {
    Link
} from "react-router-dom";

const SideBar = () => {
    return (
        // <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            {/* <!-- Sidebar - Brand --> */}
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Coding.Id</div>
            </a>

            {/* <!-- Divider --> */}
            <hr class="sidebar-divider my-0"/>

            {/* <!-- Nav Item - Dashboard --> */}
            <li class="nav-item active">
                <Link class="nav-link" to="/">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </Link>
            </li>

            {/* <!-- Divider --> */}
            <hr class="sidebar-divider"/>

            {/* <!-- Heading --> */}
            <div class="sidebar-heading">
                Master
            </div>

            {/* <!-- Nav Item - Pages Collapse Menu --> */}
            <li class="nav-item">
                <Link class="nav-link collapsed" to='/client'>
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Client List</span>
                </Link>
            </li>

            {/* <!-- Divider --> */}
            <hr class="sidebar-divider my-0"/>

            {/* <!-- Nav Item  --> */}
            <li class="nav-item">
                <Link class="nav-link collapsed" to='/Message'>
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Message</span>
                </Link>
            </li>
        </ul>
        /* <!-- End of Sidebar --> */
    )
}

export default SideBar
