import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import SideBar from './Components/SideBar'
import TopBar from './Components/TopBar'
import Footer from './Components/Footer'
import Client from "./Views/Client";
import Home from "./Views/Home"
import Message from "./Views/Message"

const App = () => {
  return (
    <Router>
      <div id="wrapper">
        <SideBar/>
        <div id="content-wrapper" class="d-flex flex-column">
          {/* <!-- Main Content --> */}
          <div id="content">

            <TopBar/>

            <Switch>
              
              <Route path="/Message">
                <Message/>
              </Route>
              <Route path="/Client">
                <Client/>
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
            
            
          </div>
          <Footer/>
        </div>
      </div>
    </Router>
  )
}

export default App
