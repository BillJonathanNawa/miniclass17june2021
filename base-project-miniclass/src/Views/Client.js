import React, {useState, useEffect} from 'react'
import axios from 'axios'

const data = [
  {
    id:1,
    name: "Bill",
    email: "Bill@gmail.com",
    number:"081234567890"
  },
  {
    id:2,
    name: "Jonathan",
    email: "Jonathan@gmail.com",
    number:"081234567890"
  },
  {
    id:3,
    name: "Wardoyo",
    email: "Wardoyo@gmail.com",
    number:"081234567890"
  }
]

function Client() {
  const [dataClient, setDataClient] = useState([])
  const [modalStatus, setModalStatus] = useState(0)
  const [form, setForm] = useState({
    id:0,
    name: "",
    email: "",
    number:""
  })

  useEffect(() => {
    getDataClient()
  },[]);

  
  const setNewForm = () => {
    setForm({
      id:0,
      name: "",
      email: "",
      number:""
    })
  }
  
  const getDataClient = ()=>{
    axios.get('http://localhost:5000/api/client/getclient')
    .then(function (response) {
      // handle success
      console.log(response.data);
      setDataClient(response.data)
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }


  const addDataClient = ()=>{
    // console.log('form', form)
    axios.post('http://localhost:5000/api/client/addClient',form)
    .then(function (response) {
      // handle success
      console.log(response.data);
      getDataClient()
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }

  const deleteDataClient =(id)=>{
    axios.post('http://localhost:5000/api/client/deleteClient/'+id)
    .then(function (response) {
      // handle success
      console.log(response.data);
      getDataClient()
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }

  const editDataClient = ()=>{
    axios.post('http://localhost:5000/api/client/updateclient', form)
    .then(function (response) {
      // handle success
      console.log(response.data);
      getDataClient()
    })
    .catch(function (error) {
      // handle error
      console.log(error);
    })
  }

  return (
    <div className="container mt-5">
        <div style={{display:'flex', justifyContent:'space-between', alignItems:'center'}}>
            <h1 style={{fontFamily:'Nunito'}}>Client List</h1>
            <div>
              <button 
                  className="btn btn-primary" 
                  data-bs-toggle="modal" 
                  data-bs-target="#exampleModal" 
                  onClick={()=>{
                    setNewForm()
                    setModalStatus(0)
                  }}> Add </button>
            </div>
        </div>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">WA Number</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
        {dataClient.map((item, index)=>
          <tr key={index}>
            <th scope="row">{index+1}</th>
            <td>{item.name}</td>
            <td>{item.email}</td>
            <td>{item.number}</td>
            <td>
              <button 
                className="btn btn-primary m-2" 
                data-bs-toggle="modal" 
                data-bs-target="#exampleModal" 
                onClick={()=>{
                  setForm(item)
                  setModalStatus(1)
                }}
                >Edit</button>
              <button className="btn btn-danger" onClick={()=>deleteDataClient(item.id)}>Delete</button>
            </td>
          </tr>
        )}
        </tbody>
      </table>

      <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">{modalStatus==0?"Add Client":"Edit Client"}</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form>
                <div className="mb-3">
                  <label for="recipient-name" className="col-form-label">Name:</label>
                  <input 
                    type="text" 
                    className="form-control" 
                    id="recipient-name" 
                    value={form.name} 
                    onChange={(e)=>setForm({...form,name:e.target.value})}
                  />
                </div>
                <div className="mb-3">
                  <label for="recipient-name" className="col-form-label">Email:</label>
                  <input 
                    type="text" 
                    className="form-control" 
                    id="recipient-email"
                    value={form.email} 
                    onChange={(e)=>setForm({...form,email:e.target.value})}
                    />
                </div>
                <div className="mb-3">
                  <label for="recipient-name" className="col-form-label">Wa Number:</label>
                  <input 
                    type="text" 
                    className="form-control" 
                    id="recipient-wanumber"
                    value={form.number} 
                    onChange={(e)=>setForm({...form,number:e.target.value})}
                    />
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={modalStatus==0?addDataClient:editDataClient}>{modalStatus==0?"Add":"Edit"}</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Client;
