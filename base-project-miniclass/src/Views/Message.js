import React, {useState} from 'react'
import Axios from 'axios'
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
      maxWidth: 300,
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    noLabel: {
      marginTop: theme.spacing(3),
    },
  }));
  
  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  
  const names = [
    'Oliver Hansen',
    'Van Henry',
    'April Tucker',
    'Ralph Hubbard',
    'Omar Alexander',
    'Carlos Abbott',
    'Miriam Wagner',
    'Bradley Wilkerson',
    'Virginia Andrews',
    'Kelly Snyder',
  ];
  
  function getStyles(name, personName, theme) {
    return {
      fontWeight:
        personName.indexOf(name) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  }
  




const Message = () => {
    
    const classes = useStyles();
    const theme = useTheme();

    const [email, setEmail] = useState("")
    const [subject, setSubject] = useState("")
    const [message, setMessage] = useState("")
    const [loading, setLoading] = useState(false)

    
    const [dataClientEmail, setDataClientEmail] = useState([])
    const [personName, setPersonName] = React.useState([]);

    const handleChange = (event) => {
        setPersonName(event.target.value);
    };

    React.useEffect(() => {
        getDataClient()
    }, [])

    
    const getDataClient = ()=>{
        Axios.get('http://localhost:5000/api/client/getclient')
        .then(function (response) {
        // handle success
            const dataListEmail = response.data.map(item => item.email)
            setDataClientEmail(dataListEmail)
        })
        .catch(function (error) {
        // handle error
        console.log(error);
        })
    }

    const sendMessage = ()=>{
        setLoading(true)

        const dataSend = {
            email:personName.join(', '),
            subject:subject,
            message:message
        }
        
        Axios.post('http://localhost:5000/api/message/sendEmail', dataSend)
        .then(res=>{
            console.log(res)
            
            setEmail("")
            setSubject("")
            setMessage("")
        })
        .catch(err=>{
            console.log(err)
        })
        .finally(()=>{
            setLoading(false)
        })
    }

    const sendMessageAll= () => {
        setLoading(true)

        const dataSend = {
            email:email,
            subject:subject,
            message:message
        }
        
        Axios.post('http://localhost:5000/api/message/sendEmailToAll', dataSend)
        .then(res=>{
            console.log(res)
            
            setEmail("")
            setSubject("")
            setMessage("")
        })
        .catch(err=>{
            console.log(err)
        })
        .finally(()=>{
            setLoading(false)
        })
    }

    return loading?
            <div className="container mt-5">
                <h5>Loading</h5>
            </div>
            :
            <div className="container mt-5">
                <div style={{display:'flex', justifyContent:'space-between', alignItems:'center'}}>
                    <h1 style={{fontFamily:'Nunito'}}>Message</h1>
                </div>

                <div class="mb-3">
                    <label 
                        for="exampleFormControlInput1" 
                        class="form-label">
                            Email address
                    </label>
                    <input 
                        type="email" 
                        class="form-control" 
                        id="exampleFormControlInput1" 
                        placeholder="name@example.com"
                        value={email}
                        onChange={(event)=> setEmail(event.target.value)}/>
                </div>

                <div class="mb-3">
                    <InputLabel id="demo-mutiple-name-label">Name</InputLabel>
                    <Select
                        labelId="demo-mutiple-name-label"
                        id="demo-mutiple-name"
                        multiple
                        value={personName}
                        onChange={handleChange}
                        input={<Input />}
                        MenuProps={MenuProps}
                        >
                        {dataClientEmail.map((email) => (
                            <MenuItem key={email} value={email} style={getStyles(email, personName, theme)}>
                            {email}
                            </MenuItem>
                        ))}
                        </Select>
                </div>

                <div class="mb-3">
                    <label 
                        for="exampleFormControlInput1" 
                        class="form-label">
                            Subject
                    </label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="exampleFormControlInput2" 
                        placeholder="Email Subject"
                        value={subject}
                        onChange={(event)=> setSubject(event.target.value)}/>
                </div>

                <div class="mb-3">
                    <label 
                        for="exampleFormControlTextarea1" 
                        class="form-label">
                            Message
                    </label>
                    <textarea 
                        class="form-control" 
                        id="exampleFormControlTextarea1" 
                        rows="5"
                        value={message}
                        onChange={(event)=> setMessage(event.target.value)}>
                    </textarea>
                </div>

                <button 
                    type="button" 
                    class="btn btn-primary"
                    onClick={sendMessage}
                    style={{marginRight:10}}>
                        Send
                </button>

                <button 
                    type="button" 
                    class="btn btn-primary"
                    onClick={sendMessageAll}>
                        Send All
                </button>
            </div>
}

export default Message
