const {Router, response} = require('express')
const Email = require('../smtp/EmailTransport')
const SQL = require('../Database/MySqlConfig')
const Mail = require('nodemailer/lib/mailer')

const MessageRouter = Router()

MessageRouter.post('/sendEmail', (req, res)=>{
    const {email, subject, message} = req.body

    if(email && subject && message){
        const dataEmail = {
            from: 'miniclasssmtp@gmail.com', // sender address
            to: email, // list of receivers
            subject: subject, // Subject line
            text: message, // plain text body
            // html: "<b>Hello world?</b>", // html body
        }
    
        Email.sendMail(dataEmail)
        .then(response=>{
            res.send({message:'Message Sended', response})
        })
        .catch(err=>{
            res.status(500).send('Failed')
        })
    }else{
        res.status(400).send({message:'Bad Request'})
    }
})


MessageRouter.post('/sendEmailToAll', (req, res)=>{
    const {subject, message} = req.body

    if(subject && message){
        SQL.query('SELECT email FROM Ms_client',(err, rows, fields)=>{

            const stringTo = rows.map(item => item.email).join(', ')
            
            const dataEmail = {
                from: 'miniclasssmtp@gmail.com', // sender address
                to: stringTo, // list of receivers
                subject: subject, // Subject line
                text: message, // plain text body
                // html: "<b>Hello world?</b>", // html body
            }
        
            Email.sendMail(dataEmail)
            .then(response=>{
                res.send({message:'Message Sended', response})
            })
            .catch(err=>{
                res.status(500).send('Failed')
            })
        })
    }else{
        res.status(400).send({message:'Bad Request'})
    }
})

module.exports = MessageRouter