const {Router} = require('express')
const MySql = require('../Database/MySqlConfig')
const router = Router()


router.get('/', (req, res) => {
    res.send('Hello World!')
})

//get data client
router.get('/getClient', (req, res) => {
    try{
        MySql.query('SELECT * FROM ms_client', (error, results, fields)=>{
            if (error) throw error;
            res.status(200).send(results)
        })
    }catch(err){
        res.status(500).send(err)
    }
})

//add data client
router.post('/addClient',(req,res)=>{
    const data = req.body
    try{
        MySql.query(`INSERT INTO ms_client VALUE (DEFAULT, '${data.name}', '${data.email}', '${data.number}')`, (error, results, fields)=>{
            if (error) throw error;
            res.status(201).send('Data Creater')
        })
    }catch(err){
        console.log(err)
        res.status(500).send(err)
    }
})

//delete data client
router.post('/deleteClient/:id',(req,res)=>{
    const data = req.params
    try{
        MySql.query(`DELETE FROM ms_client WHERE id = ${data.id}`, (error, results, fields)=>{
            if (error) throw error;
            res.status(200).send('Data Deleted')
        })
    }catch(err){
        console.log(err)
        res.status(500).send(err)
    }
})


//update data client
router.post('/updateclient',(req,res)=>{
    const data = req.body
    try{
        MySql.query(`UPDATE ms_client
                    SET name = '${data.name}' , email = '${data.email}', number = '${data.number}'
                    WHERE id = ${data.id}`, 
        (error, results, fields)=>{
            if (error) throw error;
            res.status(200).send('Data Updated')
        })
    }catch(err){
        console.log(err)
        res.status(500).send(err)
    }
})

module.exports = router