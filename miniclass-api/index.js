const express = require('express')
const path = require('path')

const cors = require('cors')

const app = express()
const port = 5000

app.use(cors())

//Body Parser Middleware    
app.use(express.json())
app.use(express.urlencoded({extended:false}))


//setStatic Folder
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/client', require('./src/routes/ClientRoute'))
app.use('/api/message', require('./src/routes/MessageRoute'))


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})